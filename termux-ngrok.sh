#!/bin/dash
# Create by Sociedad Anonima
atualizar='\033[1;32m'
echo "$atualizar Atualizando Repositorio..."
apt update && upgrade -y
apt install -y wget
clear
k='\033[01;32m'
echo

sleep 3
	setterm -foreground red

	figlet Create by Sociedad Anonima
echo " + Apoyando La Libertad De Expresión +"
sleep 5

echo "¿Quiere  instalar Ngrok? [Y/n]"
read opcao
case $opcao in
y)
echo
echo "Descargando  Termux-ngrok..."
case `dpkg --print-architecture` in
aarch64)
    architectureURL="arm64" ;;
arm)
    architectureURL="arm" ;;
armhf)
    architectureURL="armhf" ;;
amd64)
    architectureURL="amd64" ;;
i*86)
    architectureURL="i386" ;;
x86_64)
    architectureURL="amd64" ;;
*)
    echo "Arquitectura Desconocida"
esac

wget "https://github.com/tchelospy/NgrokTest/blob/master/ngrok-stable-linux-${architectureURL}.zip?raw=true" -O ngrok.zip
unzip ngrok.zip
cat ngrok > /data/data/com.termux/files/usr/bin/ngrok
chmod 700 /data/data/com.termux/files/usr/bin/ngrok
rm ngrok ngrok.zip
clear
echo "${k}███╗   ██╗ ██████╗ ██████╗  ██████╗ ██╗  ██╗";
echo "${k}████╗  ██║██╔════╝ ██╔══██╗██╔═══██╗██║ ██╔╝";
echo "${k}██╔██╗ ██║██║  ███╗██████╔╝██║   ██║█████╔╝ ";
echo "${k}██║╚██╗██║██║   ██║██╔══██╗██║   ██║██╔═██╗ ";
echo "${k}██║ ╚████║╚██████╔╝██║  ██║╚██████╔╝██║  ██╗";
echo "${k}╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝";
echo
echo "Ejemplo de un comando  (ngrok http 80\nOu ngrok para ajuda)"
;;

n)
clear
echo " Lo sentimios, Ngrok no pudo instalarse correctamente"
echo
esac
